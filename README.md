# getting-started-kubernetes

Getting Started with Kubernetes
by Nigel Poulton

Kubernetes is arguably the most important container management technology in the world. This course will teach you the theory and practical skills required to get you up and running as fast as possibl